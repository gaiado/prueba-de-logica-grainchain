window.matrix = [];

var turnAll = function (val) {
    var bulbs = document.getElementsByName('bulb');
    for (let bulb of bulbs) {
        if (bulb.checked != val) {
            bulb.click();
        }
    }
};


var toggleLight = function (self) {
    let [x, y] = self.parentNode.id.split('x');
    let spaces = window.matrix[x][y].spaces;

    if (self.checked) {
        for (let space of spaces) {
            let item = document.getElementById(space);
            if (item.className == 'item space') {
                item.className = 'item space level-1';
            } else if (item.className == 'item space level-1') {
                item.className = 'item space level-2';
            }
        }
    } else {
        for (let space of spaces) {
            let item = document.getElementById(space);
            if (item.className == 'item space level-2') {
                item.className = 'item space level-1';
            } else if (item.className == 'item space level-1') {
                item.className = 'item space';
            }
        }
    }
};

var cellTemplate = function (cell) {
    var html = '';
    html += '<div class="col">';
    html += '<div class="ratio ratio-1x1"><label id="' + cell.id + '" class="item ' + cell.type + '">';
    if (cell.type == 'bulb') {
        html += '<input name="bulb" onclick="toggleLight(this)" type="checkbox"/>';
        html += '<span/>';
    }
    html += '</label></div>';
    html += '</div>';
    return html;
};

var rowTemplate = function (row) {
    var html = '<div class="row g-0">';
    for (const cell of row) {
        html += cellTemplate(cell);
    }
    html += '</div>';
    return html;
};

var renderMatrix = function (data) {
    var html = '';
    var rows = data.rows;
    for (const row of rows) {
        html += rowTemplate(row);
    }
    document.getElementById('preview').innerHTML = html;
    document.getElementById('spaces').innerText = data.spaces;
    document.getElementById('walls').innerText = data.walls;
    document.getElementById('cells').innerText = data.walls + data.spaces;
    document.getElementById('bulbs').innerText = data.bulbs;
    window.matrix = rows;
};

var inputFile;

(function () {
    inputFile = document.getElementById('file');
    inputFile.addEventListener('change', function () {
        var file = inputFile.files[0];
        const fileMb = file.size / 1024;
        if (fileMb > 128) {
            inputFile.value = null;
            document.getElementById('mensaje').innerText = 'El archivo supera el tamaño máximo permitido';
            new window.bootstrap.Toast(document.getElementById('liveToast')).show();
        } else if (file.type != 'text/plain') {
            inputFile.value = null;
            document.getElementById('mensaje').innerText = 'Solo se permiten archivos de téxto plano';
            new window.bootstrap.Toast(document.getElementById('liveToast')).show();
        }
    });
    document.getElementById('upload').onclick = function () {
        var data = new FormData();
        data.append('file', inputFile.files[0]);

        window.axios.put('/calcular', data)
            .then(function (res) {
                renderMatrix(res.data);
            })
            .catch(function (err) {
                document.getElementById('mensaje').innerText = err.response.data.error;
                new window.bootstrap.Toast(document.getElementById('liveToast')).show();
            });
    };
})();