var express = require('express');
var router = express.Router();
var fs = require('fs');
var Matrix = require('../models/matrix');

var getMatrix = function (array) {
    var matrix = new Matrix();
    for (let i in array) {
        if (array[i]) {
            matrix.push([...array[i]]);
        }
    }
    return matrix.getData();
};

router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

router.get('/load-matrix', function (req, res) {
    var buf = fs.readFileSync('public/files/matrix.txt', 'utf8');
    var array = buf.toString('utf8').split('\n');
    let data = getMatrix(array);
    res.end('renderMatrix(' + JSON.stringify(data) + ')');
});

router.put('/calcular', function (req, res) {
    var buf = Buffer.from(req.files.file.data);
    var array = buf.toString('utf8').split('\n');
    try {
        let data = getMatrix(array);
        //solo si el archivo es valido se guarda
        fs.writeFileSync('public/files/matrix.txt', buf);
        res.end(JSON.stringify(data));
    } catch (error) {
        res.status(500).end(JSON.stringify({ error: 'Matriz inválida' }));
    }

});

module.exports = router;
