const Matrix = require('../models/matrix');

const data1 = [
    [0, 0, 0, 1, 0, 1, 1, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 1, 1, 1],
    [0, 1, 1, 1, 1, 0, 0, 0],
    [0, 1, 0, 0, 1, 0, 0, 0],
    [0, 1, 0, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 1, 0, 1, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [0, 1, 1, 0, 1, 1, 0, 1],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 1, 0, 0, 1, 1],
    [0, 0, 1, 0, 0, 0, 0, 0]
];
const resul1 = { 'bulbs': 15, 'spaces': 62, 'walls': 34, 'rows': [[{ 'id': '0x0', 'type': 'space', 'spaces': [] }, { 'id': '0x1', 'type': 'bulb', 'spaces': ['0x0', '0x2'] }, { 'id': '0x2', 'type': 'space', 'spaces': [] }, { 'id': '0x3', 'type': 'wall', 'spaces': [] }, { 'id': '0x4', 'type': 'space', 'spaces': [] }, { 'id': '0x5', 'type': 'wall', 'spaces': [] }, { 'id': '0x6', 'type': 'wall', 'spaces': [] }, { 'id': '0x7', 'type': 'wall', 'spaces': [] }], [{ 'id': '1x0', 'type': 'space', 'spaces': [] }, { 'id': '1x1', 'type': 'wall', 'spaces': [] }, { 'id': '1x2', 'type': 'bulb', 'spaces': ['0x2', '2x2', '1x7', '1x6', '1x5', '1x4', '1x3'] }, { 'id': '1x3', 'type': 'space', 'spaces': [] }, { 'id': '1x4', 'type': 'space', 'spaces': [] }, { 'id': '1x5', 'type': 'space', 'spaces': [] }, { 'id': '1x6', 'type': 'space', 'spaces': [] }, { 'id': '1x7', 'type': 'space', 'spaces': [] }], [{ 'id': '2x0', 'type': 'space', 'spaces': [] }, { 'id': '2x1', 'type': 'wall', 'spaces': [] }, { 'id': '2x2', 'type': 'space', 'spaces': [] }, { 'id': '2x3', 'type': 'space', 'spaces': [] }, { 'id': '2x4', 'type': 'bulb', 'spaces': ['0x4', '1x4', '2x2', '2x3'] }, { 'id': '2x5', 'type': 'wall', 'spaces': [] }, { 'id': '2x6', 'type': 'wall', 'spaces': [] }, { 'id': '2x7', 'type': 'wall', 'spaces': [] }], [{ 'id': '3x0', 'type': 'space', 'spaces': [] }, { 'id': '3x1', 'type': 'wall', 'spaces': [] }, { 'id': '3x2', 'type': 'wall', 'spaces': [] }, { 'id': '3x3', 'type': 'wall', 'spaces': [] }, { 'id': '3x4', 'type': 'wall', 'spaces': [] }, { 'id': '3x5', 'type': 'bulb', 'spaces': ['7x5', '6x5', '5x5', '4x5', '3x7', '3x6'] }, { 'id': '3x6', 'type': 'space', 'spaces': [] }, { 'id': '3x7', 'type': 'space', 'spaces': [] }], [{ 'id': '4x0', 'type': 'space', 'spaces': [] }, { 'id': '4x1', 'type': 'wall', 'spaces': [] }, { 'id': '4x2', 'type': 'bulb', 'spaces': ['7x2', '6x2', '5x2', '4x3'] }, { 'id': '4x3', 'type': 'space', 'spaces': [] }, { 'id': '4x4', 'type': 'wall', 'spaces': [] }, { 'id': '4x5', 'type': 'space', 'spaces': [] }, { 'id': '4x6', 'type': 'space', 'spaces': [] }, { 'id': '4x7', 'type': 'bulb', 'spaces': ['3x7', '7x7', '6x7', '5x7', '4x5', '4x6'] }], [{ 'id': '5x0', 'type': 'space', 'spaces': [] }, { 'id': '5x1', 'type': 'wall', 'spaces': [] }, { 'id': '5x2', 'type': 'space', 'spaces': [] }, { 'id': '5x3', 'type': 'bulb', 'spaces': ['4x3', '5x2'] }, { 'id': '5x4', 'type': 'wall', 'spaces': [] }, { 'id': '5x5', 'type': 'space', 'spaces': [] }, { 'id': '5x6', 'type': 'wall', 'spaces': [] }, { 'id': '5x7', 'type': 'space', 'spaces': [] }], [{ 'id': '6x0', 'type': 'space', 'spaces': [] }, { 'id': '6x1', 'type': 'wall', 'spaces': [] }, { 'id': '6x2', 'type': 'space', 'spaces': [] }, { 'id': '6x3', 'type': 'wall', 'spaces': [] }, { 'id': '6x4', 'type': 'wall', 'spaces': [] }, { 'id': '6x5', 'type': 'space', 'spaces': [] }, { 'id': '6x6', 'type': 'wall', 'spaces': [] }, { 'id': '6x7', 'type': 'space', 'spaces': [] }], [{ 'id': '7x0', 'type': 'bulb', 'spaces': ['0x0', '1x0', '2x0', '3x0', '4x0', '5x0', '6x0', '9x0', '8x0', '7x3', '7x2', '7x1'] }, { 'id': '7x1', 'type': 'space', 'spaces': [] }, { 'id': '7x2', 'type': 'space', 'spaces': [] }, { 'id': '7x3', 'type': 'space', 'spaces': [] }, { 'id': '7x4', 'type': 'wall', 'spaces': [] }, { 'id': '7x5', 'type': 'space', 'spaces': [] }, { 'id': '7x6', 'type': 'space', 'spaces': [] }, { 'id': '7x7', 'type': 'space', 'spaces': [] }], [{ 'id': '8x0', 'type': 'space', 'spaces': [] }, { 'id': '8x1', 'type': 'wall', 'spaces': [] }, { 'id': '8x2', 'type': 'wall', 'spaces': [] }, { 'id': '8x3', 'type': 'bulb', 'spaces': ['7x3', '9x3'] }, { 'id': '8x4', 'type': 'wall', 'spaces': [] }, { 'id': '8x5', 'type': 'wall', 'spaces': [] }, { 'id': '8x6', 'type': 'space', 'spaces': [] }, { 'id': '8x7', 'type': 'wall', 'spaces': [] }], [{ 'id': '9x0', 'type': 'space', 'spaces': [] }, { 'id': '9x1', 'type': 'bulb', 'spaces': ['11x1', '10x1', '9x0', '9x3', '9x2'] }, { 'id': '9x2', 'type': 'space', 'spaces': [] }, { 'id': '9x3', 'type': 'space', 'spaces': [] }, { 'id': '9x4', 'type': 'wall', 'spaces': [] }, { 'id': '9x5', 'type': 'space', 'spaces': [] }, { 'id': '9x6', 'type': 'bulb', 'spaces': ['7x6', '8x6', '9x5', '9x7'] }, { 'id': '9x7', 'type': 'space', 'spaces': [] }], [{ 'id': '10x0', 'type': 'wall', 'spaces': [] }, { 'id': '10x1', 'type': 'space', 'spaces': [] }, { 'id': '10x2', 'type': 'bulb', 'spaces': ['9x2', '10x1'] }, { 'id': '10x3', 'type': 'wall', 'spaces': [] }, { 'id': '10x4', 'type': 'bulb', 'spaces': ['11x4', '10x5'] }, { 'id': '10x5', 'type': 'space', 'spaces': [] }, { 'id': '10x6', 'type': 'wall', 'spaces': [] }, { 'id': '10x7', 'type': 'wall', 'spaces': [] }], [{ 'id': '11x0', 'type': 'bulb', 'spaces': ['11x1'] }, { 'id': '11x1', 'type': 'space', 'spaces': [] }, { 'id': '11x2', 'type': 'wall', 'spaces': [] }, { 'id': '11x3', 'type': 'space', 'spaces': [] }, { 'id': '11x4', 'type': 'space', 'spaces': [] }, { 'id': '11x5', 'type': 'bulb', 'spaces': ['9x5', '10x5', '11x3', '11x4', '11x7', '11x6'] }, { 'id': '11x6', 'type': 'space', 'spaces': [] }, { 'id': '11x7', 'type': 'space', 'spaces': [] }]] };


const data2 = [
    [0, 0, 0, 1, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 1]
];
const resul2 = { 'bulbs': 5, 'spaces': 19, 'walls': 5, 'rows': [[{ 'id': '0x0', 'type': 'bulb', 'spaces': ['3x0', '2x0', '1x0', '0x2', '0x1'] }, { 'id': '0x1', 'type': 'space', 'spaces': [] }, { 'id': '0x2', 'type': 'space', 'spaces': [] }, { 'id': '0x3', 'type': 'wall', 'spaces': [] }, { 'id': '0x4', 'type': 'space', 'spaces': [] }, { 'id': '0x5', 'type': 'bulb', 'spaces': ['2x5', '1x5', '0x4'] }], [{ 'id': '1x0', 'type': 'space', 'spaces': [] }, { 'id': '1x1', 'type': 'wall', 'spaces': [] }, { 'id': '1x2', 'type': 'space', 'spaces': [] }, { 'id': '1x3', 'type': 'space', 'spaces': [] }, { 'id': '1x4', 'type': 'bulb', 'spaces': ['0x4', '3x4', '2x4', '1x2', '1x3', '1x5'] }, { 'id': '1x5', 'type': 'space', 'spaces': [] }], [{ 'id': '2x0', 'type': 'space', 'spaces': [] }, { 'id': '2x1', 'type': 'wall', 'spaces': [] }, { 'id': '2x2', 'type': 'space', 'spaces': [] }, { 'id': '2x3', 'type': 'bulb', 'spaces': ['1x3', '3x3', '2x2', '2x5', '2x4'] }, { 'id': '2x4', 'type': 'space', 'spaces': [] }, { 'id': '2x5', 'type': 'space', 'spaces': [] }], [{ 'id': '3x0', 'type': 'space', 'spaces': [] }, { 'id': '3x1', 'type': 'bulb', 'spaces': ['3x0'] }, { 'id': '3x2', 'type': 'wall', 'spaces': [] }, { 'id': '3x3', 'type': 'space', 'spaces': [] }, { 'id': '3x4', 'type': 'space', 'spaces': [] }, { 'id': '3x5', 'type': 'wall', 'spaces': [] }]] };

describe('Calcula mejor matriz', () => {
    it('Calcula matriz 8x12', () => {
        var matrix = new Matrix();
        for (let row of data1) {
            matrix.push([...row]);
        }
        expect(matrix.getData()).toEqual(resul1);
    });

    it('Calcula matriz 6x4', () => {
        var matrix = new Matrix();
        for (let row of data2) {
            matrix.push([...row]);
        }
        expect(matrix.getData()).toEqual(resul2);
    });
});