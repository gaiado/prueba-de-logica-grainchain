module.exports = class Cell {
    constructor(value, x, y, matrix) {
        this.x = x;
        this.y = y;
        this.id = x + 'x' + y;
        this.value = value;
        this.matrix = matrix;
        this.isBulb = false;// es el emisor de luz
        this.isOn = false; // saber si se encuentra iluminado por un foco
        this.isWall = value == '1';//si es valor 1 es una pared;
        this.spaces = [];

        //objeto literal para las formulas de directions y sus asociaciones
        this.directions = {
            top: {
                cell: null, //valor para la celda asociada
                isAsociated: false, //bandera para saber si ya fue asocisAsociatedda
                j: this.x - 1,
                k: this.y
            },
            bottom: {
                cell: null,
                isAsociated: false,
                j: this.x + 1,
                k: this.y
            },
            left: {
                cell: null,
                isAsociated: false,
                j: this.x,
                k: this.y - 1
            },
            right: {
                cell: null,
                isAsociated: false,
                j: this.x,
                k: this.y + 1
            }
        };
    }

    //obtener la celda asociada si no es pared, solo se localiza la primera vez para ahorrar calculos
    getAsociated(dir) {
        let direction = this.directions[dir];
        if (direction.isAsociated)
            return direction.cell;
        let cell = this.matrix.getCellAt(direction.j, direction.k);
        direction.isAsociated = true;
        if (cell && !cell.isWall) {
            return direction.cell = cell;
        }
    }

    //calcula la cantidad de celdas que ese foco podria iluminar, no cuenta los espacios ya iluminados
    calculate(dir, turnOn = false) {
        let cell = this.getAsociated(dir);
        if (!cell) return 0;
        let num = cell.calculate(dir, turnOn) + +!cell.isOn;
        //si se manda un bulb, se enciende y se puede saber quien lo ilumina
        if (turnOn) {
            cell.isOn = true;
            turnOn.spaces.push(cell.id);
        }
        return num;
    }


    //ilumina y regresa el numero de las celdas que fueron iluminadas
    turnOn(dir) {
        return this.calculate(dir, this);
    }

    //suma los calculos de area y valida si es una celda valida
    calculateArea() {
        if (this.isWall || this.isOn) {
            return 0;
        }
        return this.calculate('top') + this.calculate('bottom') + this.calculate('left') + this.calculate('right') + 1;
    }

    //cambia el estado de las celdas dentro de su area
    setBulb() {
        this.isBulb = true;//se le agrega el estado de bulb
        this.isOn = true;//se pone como isOn para no ser contabilizado despues

        //se cuentan las celdas iluminadas por la activacion de este foco
        this.matrix.ready += this.turnOn('top') + this.turnOn('bottom') + this.turnOn('left') + this.turnOn('right') + 1;
    }

    getType() {
        if (this.isBulb)
            return 'bulb';
        else if (this.isWall)
            return 'wall';
        return 'space';
    }

    getData = () => ({
        id: this.id,
        type: this.getType(),
        spaces: this.spaces
    });
};