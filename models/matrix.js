const Cell = require('./cell');

//clase para matriz asociativa
module.exports = class Matrix {
    constructor() {
        this.rows = [];
        this.width = 0;
        this.height = 0;
        this.spaces = 0;
        this.ready = 0;
        this.bulbs = 0;
        this.walls = 0;
    }

    push(cells) {
        this.rows.push([]);
        for (let i in cells) {
            let cell = new Cell(cells[i], this.height, +i, this);
            if (cell.isWall)
                this.walls++;
            else
                this.spaces++;
            this.rows[this.height].push(cell);
        }
        if (this.width < cells.length) this.width = cells.length;
        this.height++;
    }

    getCellAt(x, y) {
        let fila = this.rows[x];
        if (fila) {
            return fila[y];
        }
    }

    //es mejor solo obtener el maximo que ordenar, esto por el numero de iteraciones
    getBestCell() {
        let best = this.rows[0][0];
        for (let i = 0; i < this.height; i++) {
            for (let j = 0; j < this.width; j++) {
                let bulb = this.rows[i][j];
                //solo calcular sobre los focos apagados
                if (!bulb.isOn && best.calculateArea() < bulb.calculateArea())
                    best = bulb;
            }
        }
        return best;
    }

    getData() {

        while (this.ready < this.spaces) {
            let cell = this.getBestCell();
            cell.setBulb();
            this.bulbs++;
        }

        let rows = [];
        for (let i = 0; i < this.height; i++) {
            let row = [];
            for (let j = 0; j < this.width; j++) {
                let cell = this.getCellAt(i, j);
                row.push(cell.getData());
            }
            rows.push(row);
        }

        return {
            bulbs: this.bulbs,
            spaces: this.spaces,
            walls: this.walls,
            rows
        };
    }
};