# Prueba de Lógica GrainChain

Demo: [link](https://prueba-de-logica-grainchain.73er8g4i0s28u.us-east-1.cs.amazonlightsail.com)

### Instalación y ejecución
El codigo puede ser instalado y ejecutado de 3 diferentes formas.
#### a) Repositorio

##### Requisitos previos

1. Node.js 16+
2. Git

##### Clonar repositorio

```
git clone https://gitlab.com/gaiado/prueba-de-logica-grainchain.git
cd prueba-de-logica-grainchain
```
##### Instalación de dependencias
```
npm install 
```

##### Ejecutar aplicación
```
npm start
```
##### Acceder a traves de un navegador web

``
http://localhost:3000
``

#### b) Dockerfile

##### Requisitos previos

1. Git
2. Docker Desktop

##### Clonar repositorio

```
git clone https://gitlab.com/gaiado/prueba-de-logica-grainchain.git
cd prueba-de-logica-grainchain
```
##### Construir imágen de docker
```
docker build . -t prueba-de-logica-grainchain   
```

##### Ejecutar contenedor
```
docker run -d -p 3000:3000 prueba-de-logica-grainchain
```
##### Acceder a traves de un navegador web

``
http://localhost:3000
``
#### c) Docker hub

##### Requisitos previos

1. Docker Desktop

##### Ejecutar contenedor
```
docker run -d -p 3000:3000 gaiado/prueba-de-logica-grainchain
```
##### Acceder a traves de un navegador web

``
http://localhost:3000
``

### Pruebas unitarias
Para la ejecución de pruebas se utiliza [Jasmine](https://jasmine.github.io/)

##### Requisitos previos

1. Node.js 16+

##### Instalación de dependencias de desarrollo
```
npm install -d
```

##### Ejecutar pruebas automatizadas
```
npm run test
```
